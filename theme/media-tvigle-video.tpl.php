<?php
/**
 * @file
 *
 * Template file for theme('media_tvigle_video').
 *
 * Variables available:
 *  $uri - The media uri for the Tvigle video (e.g., tvigle://v/123456).
 *  $video_id - The unique identifier of the Tvigle video (e.g., 123456).
 *  $id - The file entity ID (fid).
 *  $url - The full url including query options for the Tvigle iframe.
 *  $options - An array containing the Media Tvigle formatter options.
 *  $api_id_attribute - An id attribute if the Javascript API is enabled;
 *  otherwise NULL.
 *  $width - The width value set in Media: Tvigle file display options.
 *  $height - The height value set in Media: Tvigle file display options.
 *  $title - The Media: Tvigle file's title.
 *  $alternative_content - Text to display for browsers that don't support
 *  iframes.
 *
 */

?>
<div class="<?php print $classes; ?> media-tvigle-<?php print $id; ?>">
  <iframe class="media-tvigle-player" width="<?php print $width; ?>" height="<?php print $height; ?>" title="<?php print $title; ?>" src="<?php print $url; ?>" frameborder="no" scrolling="no" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""><?php print $alternative_content; ?></iframe>
</div>
