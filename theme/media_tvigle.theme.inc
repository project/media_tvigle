<?php

/**
 * @file
 * Theme and preprocess functions for Media: Tvigle.
 */

/**
 * Preprocess function for theme('media_tvigle_video').
 */
function media_tvigle_preprocess_media_tvigle_video(&$variables) {
  $wrapper = file_stream_wrapper_get_instance_by_uri($variables['uri']);
  $parts = $wrapper->get_parameters();
  $variables['video_id'] = check_plain($parts['v']);

  // Make the file object available.
  $file = file_uri_to_object($variables['uri']);
  $variables['id'] = $file->fid;
  list($x, $y) = explode(':', $file->metadata['aspect_ratio']);

  if (isset($variables['options']['width'])) {
    $variables['width'] = $variables['options']['width'];
    $variables['height'] = ceil($variables['width'] * $y / $x);
  }

  // Add template variables for accessibility.
  $variables['title'] = check_plain($file->filename);
  $variables['alternative_content'] = $variables['title'];

  // Build the iframe URL.
  $variables['url'] = url($wrapper->getExternalUrl(), array('query' => array('width' => $variables['width'], 'height' => $variables['height']), 'external' => TRUE));
}
