<?php
/**
 * @file
 * Administration pages for the Media: Tvigle module.
 */

function media_tvigle_settings_form() {
  $form['media_tvigle_access_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Access token'),
    '#default_value' => variable_get('media_tvigle_access_token', ''),
    '#description' => t('Tvigle Cloud access token / secret key.'),
  );

  $form['media_tvigle_soap_login'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Tvigle Login'),
    '#default_value' => variable_get('media_tvigle_soap_login', ''),
    '#size'          => 50,
    '#maxlength'     => 100,
  );
  $form['media_tvigle_soap_password'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Tvigle Password'),
    '#default_value' => variable_get('media_tvigle_soap_password', ''),
    '#size'          => 50,
    '#maxlength'     => 100,
  );

  return system_settings_form($form);
}
