<?php
/**
 *  @file
 *  Extends the MediaReadOnlyStreamWrapper class to handle Tvigle videos.
 */

/**
 *  Create an instance like this:
 *  $tvigle = new MediaTvigleStreamWrapper('tvigle://v/[video-code]');
 */
class MediaTvigleStreamWrapper extends MediaReadOnlyStreamWrapper {
  protected $base_url = 'http://cloud.tvigle.ru/video/';

  /**
   * Handles parameters on the URL string.
   */
  public function interpolateUrl() {
    $file = file_uri_to_object($this->uri);
    if (isset($file->metadata['src'])) {
      return $file->metadata['src'];
    }
    elseif ($parameters = $this->get_parameters()) {
      return $this->base_url . $parameters['v'];
    }
  }

  static function getMimeType($uri, $mapping = NULL) {
    return 'video/tvigle';
  }

  function getOriginalThumbnailPath() {
    $file = file_uri_to_object($this->uri);
    return $file->metadata['thumbnail'];
  }

  function getLocalThumbnailPath() {
    $parts = $this->get_parameters();
    // There's no need to hide thumbnails, always use the public system rather
    // than file_default_scheme().
    $local_path = 'public://media-tvigle/' . check_plain($parts['v']) . '.jpg';

    if (!file_exists($local_path)) {
      $dirname = drupal_dirname($local_path);
      file_prepare_directory($dirname, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      $response = drupal_http_request($this->getOriginalThumbnailPath());

      if (!isset($response->error)) {
        file_unmanaged_save_data($response->data, $local_path, TRUE);
      }
      else {
        @copy($this->getOriginalThumbnailPath(), $local_path);
      }
    }

    return $local_path;
  }
}
