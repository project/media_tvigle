<?php
/**
 * @file
 * Extends the MediaInternetBaseHandler class to handle Tvigle videos.
 */

/**
 * Implementation of MediaInternetBaseHandler.
 *
 * @see hook_media_internet_providers().
 */
class MediaInternetTvigleHandler extends MediaInternetBaseHandler {
  public function parse($embedCode) {
    if ($id = $this->getID($embedCode)) {
      return file_stream_wrapper_uri_normalize('tvigle://v/' . $id);
    }
  }

  public function getID($embedCode) {
    // http://cloud.tvigle.ru/client/videos/5189631/
    // tvigle/5189631
    $patterns = array(
      '@cloud\.tvigle\.ru/client/videos/(\d+)@i',
      '@tvigle/(\d+)@i',
    );
    foreach ($patterns as $pattern) {
      preg_match($pattern, $embedCode, $matches);
      if (isset($matches[1])) {
        return $matches[1];
      }
    }
    return FALSE;
  }

  public function claim($embedCode) {
    if ($this->parse($embedCode)) {
      return TRUE;
    }
  }

  public function getFileObject() {
    $uri = $this->parse($this->embedCode);
    $file = file_uri_to_object($uri, TRUE);

    // Try to default the file name to the video's title.
    if (empty($file->fid) && $info = $this->getInfo()) {
      switch ($info['media_tvigle_source']) {
        case 'cloud.tvigle.ru':
          $file->filename = truncate_utf8($info['name'], 255);
          $file->metadata['duration'] = $info['duration_in_ms'];
          $file->metadata['aspect_ratio'] = $info['aspect_ratio'];
          $file->metadata['thumbnail'] = $info['thumbnail'];
          break;
        case 'vp.tvigle.ru':
          $file->filename = truncate_utf8($info['name'], 255);
          $file->metadata['duration'] = $info['duration'];
          $file->metadata['aspect_ratio'] = ($info['rs'] == 1 ? '16:9' : '4:3');
          $file->metadata['thumbnail'] = $info['img'];
          $file->metadata['src'] = $info['frame'];
          break;
      }
    }

    return $file;
  }

  /**
   * Returns information about the media.
   */
  public function getInfo() {
    $access_token = variable_get('media_tvigle_access_token', '');
    $id = $this->getID($this->embedCode);
    if ($access_token) {
      $info_url = url('http://cloud.tvigle.ru/api/videos/' . $id, array('query' => array('access_token' => $access_token)));
      $response = drupal_http_request($info_url);

      if (!isset($response->error)) {
        $info = drupal_json_decode($response->data);
        $info['media_tvigle_source'] = 'cloud.tvigle.ru';
        return $info;
      }
    }

    $soap_login = variable_get('media_tvigle_soap_login', '');
    $soap_password = variable_get('media_tvigle_soap_password', '');
    if ($soap_login && $soap_password && $soap = $this->soap_client($soap_login, $soap_password)) {
      $obj = $soap->VideoItem($id);
      $info = (array) $obj;
      $info['media_tvigle_source'] = 'vp.tvigle.ru';
      return $info;
    }
    else {
      // throw new Exception("Error Processing Request. (Error: {$response->code}, {$response->error})");
      throw new Exception("Error Processing Request.");
    }
  }

  public function soap_client($soap_login, $soap_password) {
    $tvigle_service_url = 'http://pub.tvigle.ru/soap/index.php?wsdl';
    $tvigle_service_options = array('login' => $soap_login, 'password' => $soap_password);
    //$tvigle_service_options = array('login' => $soap_login, 'password' => $soap_password, 'use' => SOAP_ENCODED, 'style' => SOAP_RPC, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS);
    try {
      $soap = new SoapClient($tvigle_service_url, $tvigle_service_options);
    }
    catch(SoapClient $e) {
      $soap = FALSE;
    }
    catch(SoapFault $e) {
      $soap = FALSE;
    }
    catch(Exception $e) {
      $soap = FALSE;
    }
    return $soap;
  }
}
